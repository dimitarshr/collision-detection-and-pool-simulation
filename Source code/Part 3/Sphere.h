#pragma once
#include "RigidBody.h"

class Sphere : public RigidBody {
public:
	Sphere();
	~Sphere();

	void setRadius(float radius);
	float getRadius();
	glm::vec3 getCentre();
	std::vector<std::tuple<int, int>> getBelongingCells();
	void addBelongingCell(std::tuple<int, int> cell);
	void setInvInertia();
	glm::mat3 getInvInertia();
	void scale(glm::vec3 vect);
	void setMass(float mass);

private:
	float m_radius;
	glm::vec3 m_centre;
	std::vector<std::tuple<int, int>> belongingCells;
};
#pragma once
#include "RigidBody.h"

RigidBody::RigidBody() {

}

RigidBody::~RigidBody() {

}

void RigidBody::setInvInertia() {
	glm::mat3 inertia = glm::mat3(0);

	float w = this->getMesh().getScale()[0][0] * 2.0f;
	float h = this->getMesh().getScale()[1][1] * 2.0f;
	float d = this->getMesh().getScale()[2][2] * 2.0f;
	float m = this->getMass();

	inertia[0][0] = (1.0f / 12.0f) * m * (h * h + d * d);
	inertia[1][1] = (1.0f / 12.0f) * m * (w * w + d * d);
	inertia[2][2] = (1.0f / 12.0f) * m * (w * w + h * h);

	this->m_invInertia = glm::inverse(inertia);
}

glm::mat3 RigidBody::getInvInertia() {
	return this->getRotate() * this->m_invInertia * glm::transpose(this->getRotate());
}

void RigidBody::scale(glm::vec3 vect) {
	Body::scale(vect);
	this->setInvInertia();
}

void RigidBody::setMass(float mass) {
	Body::setMass(mass);
	this->setInvInertia();
}
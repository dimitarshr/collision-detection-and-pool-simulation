#pragma once
#include <tuple>
#include "Sphere.h"

Sphere::Sphere() {
	this->m_centre = this->getMesh().getPos();
	this->m_radius = 1.0f;
}

Sphere::~Sphere() {

}

void Sphere::setRadius(float radius) {
	this->m_radius = radius;
}

float Sphere::getRadius() {
	return this->m_radius;
}


glm::vec3 Sphere::getCentre() {
	return this->getMesh().getPos();
}

std::vector<std::tuple<int, int>> Sphere::getBelongingCells() {
	return this->belongingCells;
}

void Sphere::addBelongingCell(std::tuple<int, int> cell) {
	this->belongingCells.push_back(cell);
}

void Sphere::setInvInertia() {
	glm::mat3 inertia = glm::mat3(0);

	float r = this->getRadius();
	float m = this->getMass();

	inertia[0][0] = (2.0f / 5.0f) * m * (r * r);
	inertia[1][1] = (2.0f / 5.0f) * m * (r * r);
	inertia[2][2] = (2.0f / 5.0f) * m * (r * r);

	this->m_invInertia = glm::inverse(inertia);
}

glm::mat3 Sphere::getInvInertia() {
	return this->getRotate() * this->m_invInertia * glm::transpose(this->getRotate());
}

void Sphere::scale(glm::vec3 vect) {
	Body::scale(vect);
	this->setInvInertia();
}

void Sphere::setMass(float mass) {
	Body::setMass(mass);
	this->setInvInertia();
}
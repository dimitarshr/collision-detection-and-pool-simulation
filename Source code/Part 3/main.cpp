#pragma once
// Math constants
#define _USE_MATH_DEFINES
#include <cmath>  
#include <random>

// Std. Includes
#include <string>
#include <time.h>
#include <map>
#include <time.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include "glm/ext.hpp"

// Other Libs
#include "SOIL2/SOIL2.h"

// project includes
#include "Application.h"
#include "Shader.h"
#include "Mesh.h"
#include "Particle.h"
#include "RigidBody.h"
#include "Sphere.h"

// time
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

void detectCollisionWithCushions(Sphere *sphere, Mesh poolTablePlane) {
	for (int index = 0; index <= 2; index++) {
		/*****Required to reverse the ang velocity****/
		int angIndex = 0;
		if (index == 0) {
			angIndex = 2;
		}
		/*********************************************/
		if (sphere->getPos()[index] < (poolTablePlane.getPos()[index] - poolTablePlane.getScale()[index][index]) + sphere->getRadius()) {
			sphere->setVel(index, sphere->getVel()[index] * -0.4f); // Recudes the speed as a result of the collision.
			sphere->setAngVel(angIndex, sphere->getAngVel()[angIndex] * -0.4f); // Recudes the angular speed as a result of the collision.
			sphere->setPos(index, (poolTablePlane.getPos()[index] - poolTablePlane.getScale()[index][index] + sphere->getRadius()));
		}
		else if (sphere->getPos()[index] > (poolTablePlane.getPos()[index] + poolTablePlane.getScale()[index][index]) - sphere->getRadius()) {
			sphere->setVel(index, sphere->getVel()[index] * -0.4f); // Recudes the speed as a result of the collision.
			sphere->setAngVel(angIndex, sphere->getAngVel()[angIndex] * -0.4f); // Recudes the angular speed as a result of the collision.
			sphere->setPos(index, (poolTablePlane.getPos()[index] + poolTablePlane.getScale()[index][index]) - sphere->getRadius());
		}
	}
}

void detectCollisionBetweenSpheres(Sphere *sphere, std::vector<Sphere *> &spheres, int index) {
	Sphere* mainSphere = sphere;
	for (size_t k = 0; k < spheres.size(); k++) {
		if (index != k) {
			Sphere* secondarySphere = spheres[k];
			if (glm::distance(mainSphere->getCentre(), secondarySphere->getCentre()) < (mainSphere->getRadius() + secondarySphere->getRadius())) {

				if (glm::length(mainSphere->getVel()) > 0 || glm::length(secondarySphere->getVel()) > 0) {

					float displacement = (mainSphere->getRadius() + secondarySphere->getRadius()) - glm::distance(mainSphere->getCentre(), secondarySphere->getCentre());;
					glm::vec3 n = glm::normalize(secondarySphere->getCentre() - mainSphere->getCentre());
					glm::vec3 r1 = (secondarySphere->getCentre() - mainSphere->getCentre()) / 2.0f;
					glm::vec3 r2 = (mainSphere->getCentre() - secondarySphere->getCentre()) / 2.0f;

					float mainSpherePortion = glm::length(mainSphere->getVel()) / (glm::length(mainSphere->getVel()) + glm::length(secondarySphere->getVel()));
					float secondarySpherePortion = glm::length(secondarySphere->getVel()) / (glm::length(mainSphere->getVel()) + glm::length(secondarySphere->getVel()));

					mainSphere->translate(n * -1.0f * displacement * mainSpherePortion);
					secondarySphere->translate(n * displacement * secondarySpherePortion);

					glm::vec3 vR = secondarySphere->getVel() + glm::cross(secondarySphere->getAngVel(), r2) - (mainSphere->getVel() + glm::cross(mainSphere->getAngVel(), r1));
					float d = glm::dot(vR, n);

					float e = 0.4f;
					float bottomPart = glm::dot(glm::cross(mainSphere->getInvInertia() * glm::cross(r1, n), r1) + glm::cross(secondarySphere->getInvInertia() * glm::cross(r2, n), r2), n);
					float j = (-1.0f * (1.0f + e) * d) / (1.0f / mainSphere->getMass() + 1.0f / secondarySphere->getMass() + bottomPart);

					glm::vec3 impulse = j * n;

					mainSphere->setVel(mainSphere->getVel() - (impulse / mainSphere->getMass()));
					mainSphere->setAngVel(mainSphere->getAngVel()*0.3f + mainSphere->getInvInertia() * glm::cross(glm::normalize(mainSphere->getVel()), impulse));
					secondarySphere->setVel(secondarySphere->getVel() + impulse / secondarySphere->getMass());
					secondarySphere->setAngVel(mainSphere->getAngVel() * 0.9f);
				}
			}
		}
	}
}

// main function
int main()
{
	// create application
	Application app = Application::Application();
	app.initRender();
	Application::camera.setCameraPosition(glm::vec3(0.0f, 5.0f, 20.0f));

	// time
	const double dt = 1.0f / 60.0f;
	double startTime = (GLfloat) glfwGetTime();
	double accumulator = 0.0;

	/*
	** Pool - Final project
	*/
	// Create pool table
	Mesh poolTable = Mesh::Mesh(Mesh::QUAD);
	poolTable.scale(glm::vec3(40.0f, 0.0f, 20.0f));
	poolTable.setShader(Shader("resources/shaders/physics.vert", "resources/shaders/solid_green.frag"));

	// Spheres' configurations
	int numberOfSpheres = 4;
	std::vector <Sphere*> poolBalls;
	std::map<std::tuple<float, float>, int> usedPositions;
	Mesh sphereMesh = Mesh::Mesh("resources/models/sphere.obj");
	Shader sphereShader = Shader("resources/shaders/physicsTexture.vert", "resources/shaders/physicsTexture.frag");

	int numberOfBoxesOnRowAndColumn = 1;
	float boxSize = poolTable.getScale()[0][0] / numberOfBoxesOnRowAndColumn * 2.0f;

	for (int i = 0; i < numberOfSpheres; i++) {
		Sphere *ball = new Sphere();
		ball->setMesh(sphereMesh);
		ball->getMesh().setShader(sphereShader);

		ball->setVel(glm::vec3(0.0f, 0.0f, 0.0f));
		ball->setAngVel(glm::vec3(0.0f, 0.0f, 0.0f));
		ball->setMass(10.0f);

		Gravity * gravity = new Gravity(glm::vec3(0.0f, -9.8, 0.0f));
		ball->addForce(gravity);
		KineticFriction *kFriction= new KineticFriction();
		ball->addForce(kFriction);


		poolBalls.push_back(ball);
	}

	//poolBalls[0]->setVel(glm::vec3(10, 0, 16));
	poolBalls[0]->setVel(glm::vec3(10, 0, 0));
	poolBalls[0]->setPos(glm::vec3(-20, 1, 2));
	/*The triangle of spheres*/
	// Row 1
	poolBalls[1]->setPos(glm::vec3(20, 1, 0));
	// Row 2
	poolBalls[2]->setPos(glm::vec3(22, 1, 1));
	poolBalls[3]->setPos(glm::vec3(22, 1, -1));
	/********Used only if numberOfSpheres is 16********/
	// Row 3
	//poolBalls[4]->setPos(glm::vec3(24, 1, -2));
	//poolBalls[5]->setPos(glm::vec3(24, 1, 0));
	//poolBalls[6]->setPos(glm::vec3(24, 1, 2));
	//// Row 4
	//poolBalls[7]->setPos(glm::vec3(26, 1, -3));
	//poolBalls[8]->setPos(glm::vec3(26, 1, -1));
	//poolBalls[9]->setPos(glm::vec3(26, 1, 1));
	//poolBalls[10]->setPos(glm::vec3(26, 1, 3));
	//// Row 5
	//poolBalls[11]->setPos(glm::vec3(28, 1, -4));
	//poolBalls[12]->setPos(glm::vec3(28, 1, -2));
	//poolBalls[13]->setPos(glm::vec3(28, 1, 0));
	//poolBalls[14]->setPos(glm::vec3(28, 1, 2));
	//poolBalls[15]->setPos(glm::vec3(28, 1, 4));
	/*****************************************************/
	GLfloat animationStart = (GLfloat)glfwGetTime();
	
	/*******************************************************************/
	bool impulseApplied = false;
	bool frictionImpulseApplied = false;
	int shotMultiplier = 60;

	// Game loop
	while (!glfwWindowShouldClose(app.getWindow()))
	{
		/*
		**	SIMULATION
		*/
		GLfloat newTime = (GLfloat)glfwGetTime();
		GLfloat frameTime = newTime - startTime;
		startTime = newTime;
		accumulator += frameTime;
		app.showFPS();

		while (accumulator >= dt) {
			// *3.0f makes the movements faster
			app.doMovement(dt*3.0f);

			// Waits 2 secods before the animation starts
			if ((GLfloat)glfwGetTime() - animationStart >= 2.0f) {

				for (int index = 0; index < poolBalls.size(); index++) {
					Sphere *ball = poolBalls[index];

					// Ensures that the sphere has some movements.
					if (glm::length(ball->getVel()) != 0.0f && 
					   (glm::length(ball->getAngVel()) != 0.0f || !frictionImpulseApplied)) {

						// The offset from the CoM that the sphere is to be hit.
						// In this case it is above the CoM.
						glm::vec3 hitPoint = glm::vec3(0.0f, 1.0f, 0.0f);

						// The hit on the sphere.
						if (!impulseApplied) {
							glm::vec3 impulse = ball->getVel() * shotMultiplier;
							ball->setVel(ball->getVel() + impulse / ball->getMass());

							glm::vec3 CoM = ball->getPos();
							glm::vec3 impulsePoint = CoM + hitPoint;

							glm::vec3 omega = ball->getInvInertia() * glm::cross(impulsePoint - CoM, impulse);
							ball->setAngVel(ball->getAngVel() + omega);
							impulseApplied = true;
						}
						
						// Set the velocity, based on the acceleration
						ball->setAcc(ball->applyForces(ball->getPos(), ball->getVel(), newTime, dt));
						ball->setVel(ball->getVel() + dt * ball->getAcc());
						ball->translate(dt * ball->getVel());

						/*Friction*/
						float  frictionStrength = 0.2;
						glm::vec3 frictionImpulse = glm::normalize(ball->getVel()) * frictionStrength * -1.0f;

						// The point of impulse for friction is a the very bottom of the sphere
						glm::vec3 CoM = ball->getPos();
						glm::vec3 impulsePoint = CoM + glm::vec3(0.0f, -1.0f * ball->getRadius(), 0.0f);
						glm::vec3 omega = ball->getInvInertia() * glm::cross(impulsePoint - CoM, frictionImpulse);
						ball->setAngVel(ball->getAngVel() + omega);

						glm::mat3 angVelSkew = glm::matrixCross3(ball->getAngVel());
						glm::mat3 R = glm::mat3(ball->getRotate());
						R += dt * angVelSkew * R;
						R = glm::orthonormalize(R);
						ball->setRotate(glm::mat4(R));
							
						if (glm::length(ball->getVel()) < 5.0 && frictionImpulseApplied) {
							glm::vec3 newAnglAcc = (frictionStrength * glm::length(ball->getAngVel()) * 10 * ball->getMass()) * glm::normalize(ball->getAngVel()) * -1.0f / ball->getMass();
							ball->setAngAccl(newAnglAcc);
							ball->setAngVel(ball->getAngVel() + dt * ball->getAngAcc());
						}
						else if (frictionImpulseApplied){
							glm::vec3 newAnglAcc = (0.02 * glm::length(ball->getAngVel()) * 10 * ball->getMass()) * glm::normalize(ball->getAngVel()) * -1.0f / ball->getMass();
							ball->setAngAccl(newAnglAcc);
							ball->setAngVel(ball->getAngVel() + dt * ball->getAngAcc());
						}

						if (glm::length(ball->getAngVel()) <= 0.01) {
							ball->setAngVel(glm::vec3(0.0f));
						}
						detectCollisionWithCushions(ball, poolTable);
						detectCollisionBetweenSpheres(ball, poolBalls, index);
					}
				}
				frictionImpulseApplied = true;
			}
			accumulator -= dt;
		}

		/*
		**	RENDER 
		*/		
		// clear buffer
		app.clear();

		/***********TABLE*************/
		app.draw(poolTable);
		/******************************/

		/***********SPHERES************/
		for (Sphere *ball : poolBalls)
		{
			app.draw(ball->getMesh());
		}
		/*****************************/
		app.display();
	}

	app.terminate();

	return EXIT_SUCCESS;
}


#pragma once
#include <glm/glm.hpp>
#include <iostream>

class Body;

class Force {
public:	
	Force() {}
	~Force() {}

	virtual glm::vec3 apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel);
};

/*
** GRAVITY CLASS
*/
class Gravity : public Force {
public:
	// Constructor
	Gravity() {}
	Gravity(const glm::vec3 &gravity) { m_gravity = gravity; }

	// get and set methods
	glm::vec3 getGravity() const { return m_gravity; }
	void setGravity(glm::vec3 gravity) { m_gravity = gravity; }

	// physics
	glm::vec3 apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel);

private:
	glm::vec3 m_gravity = glm::vec3(0.0f, -9.8f, 0.0f);
};

/*
** DRAG CLASS
*/
class Drag : public Force {
public:
	Drag() {}
	Drag(const float airDensity, const float dragCoeff, const float crossSection) {
		this->setAirDensity(airDensity);
		this->setDragCoeff(dragCoeff);
		this->setCrossSection(crossSection);
	}

	// physics
	glm::vec3 apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel);

	// get and set methods
	float getAirDensity() const { return this->m_airDensity; }
	void setAirDensity(float airDensity) { this->m_airDensity = airDensity; }
	
	float getDragCoeff() const { return this->m_dragCoefficient; }
	void setDragCoeff(float dragCoefficient) { this->m_dragCoefficient = dragCoefficient; }
	
	float getCrossSection() const { return this->m_crossSection; }
	void setCrossSection(float crossSection) { this->m_crossSection = crossSection; }

private:
	float m_airDensity = 1.225f;
	float m_dragCoefficient = 1.05f;
	float m_crossSection = 0.1f;
};

class Hooke : public Force {
public:
	Hooke() {}
	Hooke(Body* b1, Body* b2, float ks, float kd, float rest) {
		m_ks = ks; m_kd = kd; m_rest = rest; m_b1 = b1; m_b2 = b2;
	}

	float getSpringStiffness() const { return this->m_ks; }
	void setSpringStiffness(float springStiffness) { this->m_ks = springStiffness; }

	float getDampingCoeff() const { return this->m_kd; }
	void setDampingCoeff(float dampingCoeff) { this->m_kd = dampingCoeff; }

	float getRestLen() const { return this->m_rest; }
	void setRestLen(float restLen) { this->m_rest = restLen; }

	Body *getB1() const { return this->m_b1; }
	void setB1(Body *body) { this->m_b1 = body; }

	Body *getB2() const { return this->m_b2; }
	void setB2(Body *body) { this->m_b2 = body; }

	// Physics
	glm::vec3 apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel);

private:
	float m_ks; // spring stiffness
	float m_kd; //damping coefficient
	float m_rest; //spirnt rest length

	Body* m_b1; // pointer to the body connected to one extremity of the sprint
	Body* m_b2; // pointer to the body connected to the other extremity
};

class AerodynamicForce : public Force {
public:
	AerodynamicForce() {}
	AerodynamicForce(Body* b1, Body* b2, Body* b3) {
		m_b1 = b1;
		m_b2 = b2;
		m_b3 = b3;
	}

	Body *getB1() const { return this->m_b1; }
	void setB1(Body *body) { this->m_b1 = body; }

	Body *getB2() const { return this->m_b2; }
	void setB2(Body *body) { this->m_b2 = body; }

	Body *getB3() const { return this->m_b3; }
	void setB3(Body *body) { this->m_b3 = body; }

	glm::vec3 getWindSpeed() { return this->windSpeed; }
	void setWindSpeed(glm::vec3 windSpeed) { this->windSpeed = windSpeed; }

	// Physics
	glm::vec3 apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel);

private:
	Body *m_b1;
	Body *m_b2;
	Body *m_b3;

	glm::vec3 windSpeed;
};

class KineticFriction : public Force {
public:
	KineticFriction() {}

	glm::vec3 apply(float mass, const glm::vec3 &pos, const glm::vec3 &vel);
};
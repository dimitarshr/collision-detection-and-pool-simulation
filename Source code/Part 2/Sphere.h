#pragma once
#include "RigidBody.h"

class Sphere : public RigidBody {
public:
	Sphere();
	~Sphere();

	void setRadius(float radius);
	float getRadius();
	glm::vec3 getCentre();

private:
	float m_radius;
	glm::vec3 m_centre;
	std::vector<std::tuple<int, int>> belongingCells;
};
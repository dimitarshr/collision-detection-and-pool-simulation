#pragma once
// Math constants
#define _USE_MATH_DEFINES
#include <cmath>  
#include <random>

// Std. Includes
#include <string>
#include <time.h>
#include <map>
#include <time.h>

// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/matrix_operation.hpp>
#include "glm/ext.hpp"

// Other Libs
#include "SOIL2/SOIL2.h"

// project includes
#include "Application.h"
#include "Shader.h"
#include "Mesh.h"
#include "Particle.h"
#include "RigidBody.h"
#include "Sphere.h"

// time
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

void detectCollision(Sphere *ball, Mesh poolTablePlane) {
	for (int index = 0; index <= 2; index++) {
		// Don't check the Y axis.
		if (index != 1) {
			if (ball->getPos()[index] < (poolTablePlane.getPos()[index] - poolTablePlane.getScale()[index][index]) + ball->getRadius()) {
				ball->setVel(index, ball->getVel()[index] * -1.0f);
				ball->setPos(index, (poolTablePlane.getPos()[index] - poolTablePlane.getScale()[index][index] + ball->getRadius()));
			}
			else if (ball->getPos()[index] > (poolTablePlane.getPos()[index] + poolTablePlane.getScale()[index][index]) - ball->getRadius()) {
				ball->setVel(index, ball->getVel()[index] * -1.0f);
				ball->setPos(index, (poolTablePlane.getPos()[index] + poolTablePlane.getScale()[index][index]) - ball->getRadius());
			}
		}
	}
}

// Collision of a ball with another ball
void detectCollosionBetweenBallsOptimised(std::vector<std::vector<std::vector<Sphere *>>> &grid) {
	for (size_t col = 0; col < grid.size(); col++) {
		for (size_t row = 0; row < grid[col].size(); row++) {
			std::vector<Sphere*> box = grid[col][row];

			for (size_t k = 0; k < box.size(); k++) {
				Sphere* mainBall = box[k];

				for (size_t index = 0; index < box.size(); index++) {
					if (k != index) {
						Sphere* secondaryBall = box[index];
						if (glm::distance(mainBall->getCentre(), secondaryBall->getCentre()) < (mainBall->getRadius() + secondaryBall->getRadius())) {
							if ((glm::length(mainBall->getVel()) > 0 || glm::length(secondaryBall->getVel()) > 0)) {
								float displacement = (mainBall->getRadius() + secondaryBall->getRadius()) - glm::distance(mainBall->getCentre(), secondaryBall->getCentre());;
								glm::vec3 n = glm::normalize(secondaryBall->getCentre() - mainBall->getCentre());

								float mainBallPortion = glm::length(mainBall->getVel()) / (glm::length(mainBall->getVel()) + glm::length(secondaryBall->getVel()));
								float secondaryBallPortion = glm::length(secondaryBall->getVel()) / (glm::length(mainBall->getVel()) + glm::length(secondaryBall->getVel()));

								mainBall->translate(n * -1.0f * displacement * mainBallPortion);
								secondaryBall->translate(n * displacement * secondaryBallPortion);

								glm::vec3 vR = secondaryBall->getVel() - mainBall->getVel();
								float d = glm::dot(vR, n);

								float e = 1.0f;
								float j = (-1.0f * (1.0f + e) * d) / (1.0f / mainBall->getMass() + 1.0f / secondaryBall->getMass());

								glm::vec3 impulse = j * n;
								mainBall->setVel(mainBall->getVel() - impulse / mainBall->getMass());
								secondaryBall->setVel(secondaryBall->getVel() + impulse / secondaryBall->getMass());
							}
						}
					}
				}
			}
		}
	}
}

std::tuple<float, float> randomUniquePosition(Mesh poolTable, std::map<std::tuple<float, float>, int> &usedPositions, int i) {
	// Get the edge of the table. E.g 30 and remove 1 because of the radius.
	float xTableLimit = poolTable.getScale()[0][0] - 1.0f;
	// There are xTableLimit * 2 numbers between the limits. E.g between -29 and 29, there are 58 numbers. Then substract 29 to generate a random number between -29 and 29.
	float xPosition = rand() % ((int)xTableLimit * 2) - (xTableLimit);
	// Get the edge of the table. E.g 30 and remove 1 because of the radius.
	float zTableLimit = poolTable.getScale()[2][2] - 1.0f;
	// There are xTableLimit * 2 numbers between the limits. E.g between -29 and 29, there are 58 numbers. Then substract 29 to generate a random number between -29 and 29.
	float zPosition = rand() % ((int)zTableLimit * 2) - (zTableLimit);
	std::tuple<float, float> currentPos = std::make_tuple(xPosition, zPosition);

	while (usedPositions.find(currentPos) != usedPositions.end()) {
		xPosition = rand() % ((int)xTableLimit * 2) - (xTableLimit);
		zPosition = rand() % ((int)zTableLimit * 2) - (zTableLimit);
		currentPos = std::make_tuple(xPosition, zPosition);
	}
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(currentPos, i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos) - 1, std::get<1>(currentPos)), i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos) + 1, std::get<1>(currentPos)), i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos), std::get<1>(currentPos) - 1), i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos), std::get<1>(currentPos) + 1), i));

	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos) - 1, std::get<1>(currentPos) - 1), i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos) + 1, std::get<1>(currentPos) + 1), i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos) + 1, std::get<1>(currentPos) - 1), i));
	usedPositions.insert(std::pair<std::tuple<float, float>, int>(std::make_tuple(std::get<0>(currentPos) - 1, std::get<1>(currentPos) + 1), i));

	return currentPos;
}

// Grid - all boxes;
// currentBall - currentBall to be updated;
// numberOfRowsAndColumns - grid size (squar matrix)
// boxSize - the size of indivual box (e.g. 3)
void updateGrid(std::vector<std::vector<std::vector<Sphere *>>> &grid, Sphere* currentBall, int numberOfRowsAndColumns, int boxSize) {
	float xPosition = currentBall->getMesh().getPos()[0];
	float zPosition = currentBall->getMesh().getPos()[2];

	int column = (int)floor(xPosition / boxSize) + numberOfRowsAndColumns / 2;
	int row = (int)floor(zPosition / boxSize) + numberOfRowsAndColumns / 2;

	if (column >= 0 && column < numberOfRowsAndColumns && row >= 0 && row < numberOfRowsAndColumns) {
		grid[column][row].push_back(currentBall);
	}

	/********************Account for radius to the right and forward******************************/
	int column1 = (int)floor((xPosition + currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;
	int row1 = (int)floor((zPosition + currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;

	if (column1 > 0 && column1 < numberOfRowsAndColumns && row1 > 0 && row1 < numberOfRowsAndColumns) {
		if (column1 != column || row1 != row) {
			grid[column1][row1].push_back(currentBall);
		}
	}

	/********************Account for radius to the left and backward******************************/
	int column2 = (int)floor((xPosition - currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;
	int row2 = (int)floor((zPosition - currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;

	if (column2 >= 0 && column2 < numberOfRowsAndColumns && row2 >= 0 && row2 < numberOfRowsAndColumns) {
		if ((column2 != column || row2 != row) && (column1 != column2 || row1 != row2)) {
			grid[column2][row2].push_back(currentBall);
		}
	}

	/********************Account for radius to the right and backward******************************/
	int column3 = (int)floor((xPosition + currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;
	int row3 = (int)floor((zPosition - currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;

	if (column3 > 0 && column3 < numberOfRowsAndColumns && row3 >= 0 && row3 < numberOfRowsAndColumns) {
		if ((column3 != column || row3 != row) && (column2 != column3 || row2 != row3) && (column1 != column3 || row1 != row3)) {
			grid[column3][row3].push_back(currentBall);
		}
	}

	/********************Account for radius to the left and forward******************************/
	int column4 = (int)floor((xPosition - currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;
	int row4 = (int)floor((zPosition + currentBall->getRadius()) / boxSize) + numberOfRowsAndColumns / 2;

	if (column4 >= 0 && column4 < numberOfRowsAndColumns && row4 > 0 && row4 < numberOfRowsAndColumns) {
		if ((column4 != column || row4 != row) && (column4 != column3 || row4 != row3) && (column2 != column4 || row2 != row4) && (column1 != column4 || row1 != row4)) {
			grid[column4][row4].push_back(currentBall);
		}
	}
}

void clearGrid(int numberOfBoxesOnRowAndColumn, std::vector<std::vector<std::vector<Sphere *>>> &grid) {
	for (int column = 0; column < numberOfBoxesOnRowAndColumn; column++) {
		for (int row = 0; row < numberOfBoxesOnRowAndColumn; row++) {
			grid[column][row].clear();
		}
	}
}

// main function
int main()
{
	// create application
	Application app = Application::Application();
	app.initRender();
	Application::camera.setCameraPosition(glm::vec3(0.0f, 5.0f, 20.0f));

	// time
	const double dt = 1.0f / 60.0f;
	double startTime = (GLfloat) glfwGetTime();
	double accumulator = 0.0;

	/*
	** Pool - Final project
	*/
	srand(time(NULL));
	// Create pool table
	Mesh poolTable = Mesh::Mesh(Mesh::QUAD);
	poolTable.scale(glm::vec3(500.0f, 0.0f, 500.0f));
	poolTable.setShader(Shader("resources/shaders/physicsOptimised.vert", "resources/shaders/solid_green.frag"));

	// Spheres' configurations
	int numberOfBalls = 22000;
	std::vector <Sphere*> poolBalls;
	std::map<std::tuple<float, float>, int> usedPositions;
	Mesh sphereMesh = Mesh::Mesh("resources/models/sphere.obj");
	Shader sphereShader = Shader("resources/shaders/physicsOptimised.vert", "resources/shaders/physics.frag");

	int numberOfBoxesOnRowAndColumn = 60;
	std::vector<std::vector<std::vector<Sphere *>>> grid;
	float boxSize = poolTable.getScale()[0][0] / numberOfBoxesOnRowAndColumn * 2.0f;

	// Initialise an empty grid.
	for (int column = 0; column < numberOfBoxesOnRowAndColumn; column++) {
		std::vector<std::vector<Sphere *>> columnVector;
		for (int row = 0; row < numberOfBoxesOnRowAndColumn; row++) {
			std::vector<Sphere*> rowVector;
			columnVector.push_back(rowVector);
		}
		grid.push_back(columnVector);
	}

	for (int i = 0; i < numberOfBalls; i++) {
		Sphere *ball = new Sphere();
		ball->setMesh(sphereMesh);
		ball->getMesh().setShader(sphereShader);

		std::tuple<float, float> position = randomUniquePosition(poolTable, usedPositions, i);
		ball->translate(glm::vec3(std::get<0>(position), 1.0f, std::get<1>(position)));

		// Velocity to be between -20 and 20.
		float minSpeed = -20.0f;
		int maxSpeed = 20;
		float xVel = rand() % maxSpeed * 2 + minSpeed;
		float zVel = rand() % maxSpeed * 2 + minSpeed;
		ball->setVel(glm::vec3(xVel,0.0f, zVel));
		ball->setMass(1.0f);

		poolBalls.push_back(ball);

		updateGrid(grid, ball, numberOfBoxesOnRowAndColumn, boxSize);
	}

	GLfloat animationStart = (GLfloat)glfwGetTime();
	
	/*******************************************************************/

	// Game loop
	while (!glfwWindowShouldClose(app.getWindow()))
	{
		/*
		**	SIMULATION
		*/
		GLfloat newTime = (GLfloat)glfwGetTime();
		GLfloat frameTime = newTime - startTime;
		startTime = newTime;
		accumulator += frameTime;
		app.showFPS();

		while (accumulator >= dt) {
			app.doMovement(dt*3.0f);
			
			if ((GLfloat)glfwGetTime() - animationStart >= 2.0f) {
				clearGrid(numberOfBoxesOnRowAndColumn, grid);
				for (int index = 0; index<poolBalls.size(); index++)
				{
					Sphere *ball = poolBalls[index];
					ball->translate(dt * ball->getVel());

					float xPosition = ball->getMesh().getPos()[0];
					float zPosition = ball->getMesh().getPos()[2];
					int column = floor(xPosition / boxSize) + numberOfBoxesOnRowAndColumn / 2;
					int row = floor(zPosition / boxSize) + numberOfBoxesOnRowAndColumn / 2;

					// If there is collision with the cushions - resolve it.
					if (column <= 0 || column >= numberOfBoxesOnRowAndColumn - 1
						|| row <= 0 || row >= numberOfBoxesOnRowAndColumn - 1) {
						detectCollision(ball, poolTable);
					}

					updateGrid(grid, ball, numberOfBoxesOnRowAndColumn, boxSize);
				}
				detectCollosionBetweenBallsOptimised(grid);
			}

			accumulator -= dt;
		}

		/*
		**	RENDER 
		*/		
		// clear buffer
		app.clear();

		auto V = app.getCameraViewMatrix();
		auto P = app.getCameraProjectionMatrix();

		auto PV = P * V;

		/***********TABLE*************/
		poolTable.getShader().Use();
		GLint rotateLoc = glGetUniformLocation(poolTable.getShader().Program, "rotate");
		glUniformMatrix4fv(rotateLoc, 1, GL_FALSE, glm::value_ptr(poolTable.getRotate()));
		glBindVertexArray(poolTable.getVertexArrayObject());
		app.drawOptimised(poolTable, PV, poolTable.getNumIndices());
		glBindVertexArray(0);
		/******************************/

		/***********SPHERES************/
		glEnable(GL_CULL_FACE);
		poolBalls[0]->getMesh().getShader().Use();
		int numberIndicesSphere = poolBalls[0]->getMesh().getNumIndices();
		rotateLoc = glGetUniformLocation(poolBalls[0]->getMesh().getShader().Program, "rotate");
		glUniformMatrix4fv(rotateLoc, 1, GL_FALSE, glm::value_ptr(poolBalls[0]->getMesh().getRotate()));
		glBindVertexArray(poolBalls[0]->getMesh().getVertexArrayObject());
		for (Sphere *ball : poolBalls)
		{
			glm::vec3 cameraForward = app.camera.getForward();
			if (glm::dot(cameraForward, glm::normalize(ball->getPos() - app.camera.getPosition())) > 0.7) {
				app.drawOptimised(ball->getMesh(), PV, numberIndicesSphere);
			}
		}
		glBindVertexArray(0);
		glDisable(GL_CULL_FACE);
		/*****************************/
		app.display();
	}

	app.terminate();

	return EXIT_SUCCESS;
}


#version 410
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texCoord;
layout (location = 2) in vec3 normalIn;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 rotate;

// Offset position
uniform vec3 offsets[1000];

out vec3 normal;

void main() 
{	
	vec3 offset = offsets[gl_InstanceID];
	// Calculate screen position
	gl_Position = projection * view * model  * vec4(offset, 1.0);
  	
	 vec4 normal4 = vec4(normalIn, 1.0f);
	 vec4 wNormal = rotate * normal4;
     normal = vec3(wNormal);
}
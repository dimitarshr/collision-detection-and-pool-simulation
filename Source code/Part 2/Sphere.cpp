#pragma once
#include <tuple>
#include "Sphere.h"

Sphere::Sphere() {
	this->m_centre = this->getMesh().getPos();
	this->m_radius = 1.0f;
}

Sphere::~Sphere() {

}

void Sphere::setRadius(float radius) {
	this->m_radius = radius;
}

float Sphere::getRadius() {
	return this->m_radius;
}


glm::vec3 Sphere::getCentre() {
	return this->getMesh().getPos();
}